package cse360assign3;
/**
 * 
 * @author kellyflynn
 * Calculator is a public class that uses multiple methods to act as a calculator depending on the fuction called in the main class.
 *
 */
public class Calculator {
	
	private int total;
	private String history = "0";
	
	/**
	 * The Calculator method is the default constructor & sets default values
	 */
	public Calculator () {
		this.total = 0;  // not needed - included for clarity
	}
	
	/**
	 * 
	 * @return - returns the total value of all the actions performed in the calculator
	 */
	public int getTotal () {
		return this.total;
	}
	
	/**
	 * 
	 * @param value - the value to be added to the total.
	 */
	public void add (int value) {
		this.total = this.total + value;
		this.history += " + " + value;
	}
	
	/**
	 * 
	 * @param value - the value to be subtracted to the total.
	 */
	public void subtract (int value) {
		this.total = this.total - value;
		this.history += " - " + value;

	}
	
	/**
	 * 
	 * @param value - the value to be multiplied to the total.
	 */
	public void multiply (int value) {
		this.total = this.total * value;
		this.history += " * " + value;

	}
	
	/**
	 * 
	 * @param value - the value to divide the total by. 
	 */
	public void divide (int value) {
		
		if(value == 0){
			this.total = 0;
			}
		
		else
			this.total = this.total / value;
		
		this.history += " / " + value;
	}
	
	/**
	 * 
	 * @return - returns the string that contains all of the actions performed by the previous functions.
	 */
	public String getHistory () {
		return this.history;
	}
}